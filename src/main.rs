extern crate getopts;
extern crate graphql_parser;

use getopts::Options;
use graphql_parser::parse_schema;
use graphql_parser::schema;
use std::env;
use std::fs;
use std::path::Path;
use std::time::Instant;

fn create_enum_definitions(enum_defs: Vec<&schema::EnumType>) {
    for enum_def in enum_defs {
        println!("enum def: {}", enum_def);
    }
}

fn init_schema<P: AsRef<Path>>(file_path: P) {
    let contents = fs::read_to_string(file_path).expect("Something went wrong reading the file");

    let ast =
        parse_schema(&String::from(contents)).expect("Something went wrong parsing the schema");

    let mut enum_defs: Vec<&schema::EnumType> = Vec::new();

    for def in ast.definitions.iter() {
        match def {
            schema::Definition::TypeDefinition(type_def) => match type_def {
                schema::TypeDefinition::Enum(enum_def) => {
                    enum_defs.push(enum_def);
                }
                _ => continue,
            },
            _ => continue,
        }
    }

    create_enum_definitions(enum_defs);
}

fn print_usage(program: &str, opts: Options) {
    println!(
        "{}",
        opts.usage(&format!("Usage: {} [options] <type-def-path>", program))
    );
}

fn main() {
    let start_time = Instant::now();

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "Show this usage message.");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => panic!(e.to_string()),
    };

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }

    let data_file = args[1].clone();
    let data_path = Path::new(&data_file);

    init_schema(&data_path);

    println!(
        "Finished running in {}ms.",
        start_time.elapsed().as_millis()
    );
}
